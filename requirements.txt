Django==3.0.8
djangorestframework==3.11.0
psycopg2-binary==2.8.5
Scrapy==2.2.0
scrapy-djangoitem==1.1.1
django-cors-headers==3.4.0