import React, { Component } from "react";
import { Col, Dropdown } from 'react-bootstrap';
import { connect } from 'react-redux'
import { SELECT_AUTHOR, SET_STATS } from '../redux/types'

class Authors extends Component {
    sortProperties(obj) {
        // convert object into array
        var sortable = [];
        for (var key in obj)
            if (obj.hasOwnProperty(key))
                sortable.push([key, obj[key]]); // each item is an array in format [key, value]

        // sort items by value
        sortable.sort(function (a, b) {
            var x = a[1],
                y = b[1];
            return x > y ? -1 : x < y ? 1 : 0;
        });
        return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
    }

    toggleAuthor(event) {
        const authorKey = event.currentTarget.id
        var words

        if (this.props.selectedAuthors["all"] === true && authorKey !== "all") {
            this.props.selectAuthor(authorKey)
            this.props.setStats(this.props.authorsStats[authorKey])
            return
        } // case where 'all' is not selected, but one author is already selected and we want to select one more author
        else {
            const keys1 = Object.keys(this.props.authorsStats[authorKey])
            const keys2 = Object.keys(this.props.computedStats)

            words = keys1.concat(keys2)
        }

        words = words.filter((item, index) => words.indexOf(item) === index)

        this.props.selectAuthor(authorKey)

        var statsObj = {}

        words.map((word) => {
            if (this.props.computedStats.hasOwnProperty(word) && this.props.authorsStats[authorKey].hasOwnProperty(word)) {
                statsObj[word] = this.props.computedStats[word] + this.props.authorsStats[authorKey][word]
            }
            else if (this.props.computedStats.hasOwnProperty(word) && !this.props.authorsStats[authorKey].hasOwnProperty(word)) {
                statsObj[word] = this.props.computedStats[word]
            } else if (!this.props.computedStats.hasOwnProperty(word) && this.props.authorsStats[authorKey].hasOwnProperty(word)) {
                statsObj[word] = this.props.authorsStats[authorKey][word]
            }
        })

        const sortedStatsObj = this.sortProperties(statsObj)
        statsObj = {}
        sortedStatsObj.forEach((arr) => {
            statsObj[arr[0]] = arr[1]
        })

        this.props.setStats(statsObj)
    }

    render() {
        return (
            <Col>
                <h4>List of Authors</h4>
                <Dropdown className="mx-auto">
                    <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                        Dropdown Menu
                    </Dropdown.Toggle>

                    <Dropdown.Menu className="text-center">
                        <Dropdown.Item
                            id="all"
                            style={this.props.selectedAuthors["all"] ? { display: 'none' } : { display: 'block' }}
                            onClick={(e) => this.props.selectAuthor(e.currentTarget.id)}>
                            <span>
                                All
                            </span>
                        </Dropdown.Item>
                        {Object.entries(this.props.authorsList).map(([key, value]) => {
                            return <Dropdown.Item
                                key={key}
                                id={key}
                                style={this.props.selectedAuthors[key] ? { display: 'none' } : { display: 'block' }}
                                onClick={(e) => this.toggleAuthor(e)}>
                                <span>
                                    {value}
                                </span>
                            </Dropdown.Item>
                        })}
                    </Dropdown.Menu>
                </Dropdown>
            </Col >
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setStats: (payload) => dispatch({ type: SET_STATS, payload }),
        selectAuthor: (payload) => dispatch({ type: SELECT_AUTHOR, payload })
    }
}

const mapStateToProps = state => ({
    authorsList: state.authors.authorsList.data,
    selectedAuthors: state.authors.selectedAuthors,
    authorsStats: state.stats.authorsStats.data,
    computedStats: state.stats.computedStats
})

export default connect(mapStateToProps, mapDispatchToProps)(Authors)