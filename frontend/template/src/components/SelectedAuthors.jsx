import React, { Component } from "react";
import { Col, Card } from 'react-bootstrap';
import './SelectedAuthors.css';
import { connect } from 'react-redux'
import { SELECT_AUTHOR, SET_STATS } from '../redux/types'

class SelectedAuthors extends Component {
    componentDidUpdate() {
        if (Object.keys(this.props.selectedAuthors).every((k) => !this.props.selectedAuthors[k])) {
            this.props.selectAuthor("all")
        }
    }

    sortProperties(obj) {
        // convert object into array
        var sortable = [];
        for (var key in obj)
            if (obj.hasOwnProperty(key) && obj[key] !== 0)
                sortable.push([key, obj[key]]); // each item is an array in format [key, value]

        // sort items by value
        sortable.sort(function (a, b) {
            var x = a[1],
                y = b[1];
            return x > y ? -1 : x < y ? 1 : 0;
        });
        return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
    }

    toggleAuthor(event) {
        const authorKey = event.currentTarget.id
        var words

        const keys1 = Object.keys(this.props.authorsStats[authorKey])
        const keys2 = Object.keys(this.props.computedStats)

        words = keys1.concat(keys2)

        words = words.filter((item, index) => words.indexOf(item) === index)

        this.props.selectAuthor(authorKey)

        let statsObj = {}

        words.map((word) => {
            if (this.props.computedStats.hasOwnProperty(word)
                && this.props.authorsStats[authorKey].hasOwnProperty(word)) {
                statsObj[word] = this.props.computedStats[word] - this.props.authorsStats[authorKey][word]
            }
            else if (this.props.computedStats.hasOwnProperty(word)) {
                statsObj[word] = this.props.computedStats[word]
            }
        })

        const sortedStatsObj = this.sortProperties(statsObj)
        statsObj = {}
        sortedStatsObj.forEach((arr) => {
            statsObj[arr[0]] = arr[1]
        })

        this.props.setStats(statsObj)
    }

    render() {
        return (
            <Col>
                <h4>List of Selected Authors</h4>
                <Card className="mx-auto">
                    <Card.Body>
                        <Card.Text
                            id="all"
                            style={this.props.selectedAuthors["all"] ? { display: 'block' } : { display: 'none' }}>
                            <span>
                                All
                            </span>
                        </Card.Text>
                        {Object.entries(this.props.authorsList).map(([key, value]) => {
                            return <Card.Text
                                key={key}
                                id={key}
                                style={this.props.selectedAuthors[key] ? { display: 'block' } : { display: 'none' }}
                                onClick={(e) => this.toggleAuthor(e)}>
                                <span>
                                    {value}
                                </span>
                            </Card.Text>
                        })}
                    </Card.Body>
                </Card>
            </Col>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setStats: (payload) => dispatch({ type: SET_STATS, payload }),
        selectAuthor: (payload) => dispatch({ type: SELECT_AUTHOR, payload })
    }
}

const mapStateToProps = state => ({
    authorsList: state.authors.authorsList.data,
    selectedAuthors: state.authors.selectedAuthors,
    authorsStats: state.stats.authorsStats.data,
    computedStats: state.stats.computedStats
})

export default connect(mapStateToProps, mapDispatchToProps)(SelectedAuthors)