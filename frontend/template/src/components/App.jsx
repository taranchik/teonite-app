import React, { Component } from "react";
import { Container, Row } from 'react-bootstrap';
import Stats from './Stats.jsx';
import Authors from './Authors.jsx';
import SelectedAuthors from './SelectedAuthors.jsx';
import { fetchAuthors, fetchGeneralStats, fetchAuthorsStats } from '../redux/actions'
import { connect } from 'react-redux'

class App extends Component {
    componentDidMount() {
        this.props.fetchAuthors()
        this.props.fetchGeneralStats()
        this.props.fetchAuthorsStats()
    }

    render() {
        return (
            (this.props.generalStats && this.props.authorsStats && this.props.authorsList) ?
                <Container className="text-center" fluid>
                    <Row>
                        <h1 className="mx-auto">TEONITE APP</h1>
                    </Row>
                    <Row>
                        <Authors />
                        <Stats />
                        <SelectedAuthors />
                    </Row>
                </Container>
                : (
                    <div></div>
                )
        )
    }
}

const mapStateToProps = state => ({
    generalStats: state.stats.generalStats.isLoaded,
    authorsStats: state.stats.authorsStats.isLoaded,
    authorsList: state.authors.authorsList.isLoaded
})

const mapDispatchToProps = {
    fetchGeneralStats, fetchAuthorsStats, fetchAuthors
}

export default connect(mapStateToProps, mapDispatchToProps)(App)