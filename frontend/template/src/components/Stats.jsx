import React, { Component } from "react";
import { Col, Table } from 'react-bootstrap';
import { connect } from 'react-redux'
import { SET_STATS } from '../redux/types'

class Stats extends Component {
    componentDidUpdate() {
        if (this.props.selectedAuthors["all"] === true) {
            this.props.setStats(this.props.generalStats)
        }
    }

    render() {
        return (
            <Col>
                <h4>TABLE</h4>
                <Table className="mx-auto" bordered hover>
                    <thead className="thead-light">
                        <tr>
                            <th></th>
                            <th>Word</th>
                            <th>Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Object.entries(this.props.computedStats).map(([key, value], i) => {
                            return <tr key={key}><td>{i + 1}</td><td>{key}</td><td>{value}</td></tr>
                        })}
                    </tbody>
                </Table>
            </Col>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setStats: (payload) => dispatch({ type: SET_STATS, payload }),
    }
}

const mapStateToProps = state => ({
    generalStats: state.stats.generalStats.data,
    computedStats: state.stats.computedStats,
    selectedAuthors: state.authors.selectedAuthors
})

export default connect(mapStateToProps, mapDispatchToProps)(Stats)