import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import { compose, createStore, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga';
import { Provider } from "react-redux"
import { rootReducer } from "./redux/rootReducer"
import rootSaga from "./redux/sagas"
import { BrowserRouter as Router, Redirect } from "react-router-dom";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer,
    compose(
        applyMiddleware(sagaMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    ));

sagaMiddleware.run(rootSaga);

const app = (
    <Provider store={store}>
        <Router>
            <Redirect to="/stats/" />
            <App />
        </Router>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));