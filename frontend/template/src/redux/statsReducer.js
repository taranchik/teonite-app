import {
    FETCH_GENERAL_STATS,
    FETCH_AUTHORS_STATS,
    SET_STATS,
} from './types'

const INITIAL_STATE = {
    generalStats: {
        isLoaded: false,
        data: {}
    },
    authorsStats: {
        isLoaded: false,
        data: {}
    },
    computedStats: {}
}

export const statsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_GENERAL_STATS:
            return !state.generalStats.isLoaded ?
                { ...state, generalStats: { isLoaded: true, data: action.payload }, computedStats: action.payload } :
                { ...state, generalStats: { isLoaded: true, data: action.payload } }
        case FETCH_AUTHORS_STATS:
            return { ...state, authorsStats: { isLoaded: true, data: action.payload } }
        case SET_STATS:
            return { ...state, computedStats: action.payload }
        default:
            return state
    }
}