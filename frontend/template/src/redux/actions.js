import {
    REQUEST_GENERAL_STATS,
    REQUEST_AUTHORS_STATS,
    REQUEST_AUTHORS,
} from './types'

export function fetchGeneralStats() {
    return {
        type: REQUEST_GENERAL_STATS
    }
}

export function fetchAuthorsStats() {
    return {
        type: REQUEST_AUTHORS_STATS
    }
}

export function fetchAuthors() {
    return {
        type: REQUEST_AUTHORS
    }
}