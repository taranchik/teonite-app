import { FETCH_AUTHORS, SELECT_AUTHOR } from './types'

const INITIAL_STATE = {
    authorsList: {
        isLoaded: false,
        data: {}
    },
    selectedAuthors: {}
}

export const authorsReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_AUTHORS:
            const authorsKey = Object.keys(action.payload);
            const selectedAuthors = Object.assign({}, ...authorsKey.map((key) => ({ [key]: false })), { "all": true })
            return { ...state, authorsList: { isLoaded: true, data: action.payload }, selectedAuthors }
        case SELECT_AUTHOR:
            if (action.payload === "all" && state.selectedAuthors["all"] === false) {
                const authorsKey = Object.keys(state.authorsList.data);
                const selectedAuthors = Object.assign({}, ...authorsKey.map((key) => ({ [key]: false })), { "all": true })
                return { ...state, selectedAuthors }
            }
            else {
                return {
                    ...state, // copy state
                    selectedAuthors: {
                        ...state.selectedAuthors, // copy selectedAuthors
                        [action.payload]: !state.selectedAuthors[action.payload], // change value of author
                        ["all"]: false // set value for key "all" to false
                    }
                }
            }
        default:
            return state
    }
}