import { takeEvery, put, call, all } from 'redux-saga/effects'
import {
    REQUEST_GENERAL_STATS,
    REQUEST_AUTHORS_STATS,
    REQUEST_AUTHORS,
    FETCH_GENERAL_STATS,
    FETCH_AUTHORS_STATS,
    FETCH_AUTHORS
} from './types'

function* sagaWatcher() {
    const url = 'http://localhost:8080'

    yield takeEvery(REQUEST_GENERAL_STATS, sagaWorker, {
        type: FETCH_GENERAL_STATS,
        payload: url + '/stats/'
    })

    yield takeEvery(REQUEST_AUTHORS, sagaWorker, {
        type: FETCH_AUTHORS,
        payload: url + '/authors/'
    })

    yield takeEvery(REQUEST_AUTHORS_STATS, sagaWorker, {
        type: FETCH_AUTHORS_STATS,
        payload: url + '/authors/'
    })
}

function* sagaWorker(action) {
    try {
        const payload = yield call(async () => {
            const response = await fetch(action.payload)
            return await response.json()
        })

        switch (action.type) {
            case FETCH_GENERAL_STATS:
                yield put({ type: action.type, payload })
                break;
            case FETCH_AUTHORS_STATS:
                let authorsStats = {}
                const authorsKeys = Object.keys(payload)
                yield call(() => {
                    authorsKeys.forEach(async (key) => {
                        await fetch('http://localhost:8080/stats/' + key + '/'
                        ).then(async (data) => await data.json()
                        ).then((stats) => authorsStats[key] = stats)
                    })
                })
                yield put({ type: action.type, payload: authorsStats })
                break;
            case FETCH_AUTHORS:
                yield put({ type: action.type, payload })
                break;
            default:
                throw new Error('The action type does not match.')
        }
    } catch (e) {
        console.log(e.message)
    }
}

// single entry point to start all Sagas at once
export default function* rootSaga() {
    yield all([
        sagaWatcher()
    ])
}