import { combineReducers } from 'redux'
import { statsReducer } from './statsReducer'
import { authorsReducer } from './authorsReducer'

export const rootReducer = combineReducers({
    stats: statsReducer,
    authors: authorsReducer
})