FROM python:3.6
ENV PYTHONUNBUFFERED 1

WORKDIR /app
COPY requirements.txt /app

RUN pip3 install -r requirements.txt

EXPOSE 8080
