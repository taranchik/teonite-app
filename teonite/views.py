import json
import operator
from django.http import HttpResponse
from .models import Teonite


def stats(request):
    wordBank = {}

    for teonite_object in Teonite.objects.all().iterator():
        for word in teonite_object.words:
            if word in wordBank:
                wordBank[word] = wordBank[word] + \
                    int(teonite_object.words[word])
            else:
                wordBank[word] = int(teonite_object.words[word])

    jsonFile = json.dumps(dict(sorted(wordBank.items(
    ), key=operator.itemgetter(1), reverse=True)[:10]), indent=True)
    return HttpResponse(jsonFile, content_type='application/json')


def author_name(request, authorKey):
    wordBank = Teonite.objects.filter(
        authorKey=authorKey).values_list('words', flat=True)[0]

    for key, value in wordBank.items():
        wordBank[key] = int(value)

    jsonFile = json.dumps(dict(sorted(wordBank.items(
    ), key=operator.itemgetter(1), reverse=True)[:10]), indent=True)

    return HttpResponse(jsonFile, content_type='application/json')


def authors(request):
    response = Teonite.objects.values('authorKey', 'author')
    jsonFile = json.dumps(dict(zip((obj['authorKey'] for obj in response),
                                   (obj['author'] for obj in response))), indent=True, ensure_ascii=False)
    return HttpResponse(jsonFile, content_type='application/json')
