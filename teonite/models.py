from django.db import migrations, models
from django.contrib.postgres.fields import HStoreField


class Teonite(models.Model):
    authorKey = models.CharField('AuthorKey', max_length=30)
    author = models.CharField('Author', max_length=30)
    words = HStoreField()

    class Meta:
        app_label = 'teonite'

    def __str__(self):
        return self.author
