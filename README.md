# TEONITE Frontend App

![Project Image](https://i.imgur.com/aa9Sano.png)

> TEONITE Blog stats

---

## How To Use

### Installation

1. Clone the repositry
```
git clone https://gitlab.com/taranchik/teonite-app
```

2. Build, create, start, and attach to containers for a service
```
docker-compose up
```

### Usage

In order to fetch some data you are able to do the following actions:

1. Fetch general stats of the TEONITE blog articles (by default it is already selected)

![General stats image](https://i.imgur.com/aa9Sano.png)

2. Fetch authors of the TEONITE blog articles you have to click at the `Dropdown Menu`

![Authors list image](https://i.imgur.com/WcEirmv.png)

3. To fetch stats of the specific author, you have to select the author from the `Dropdown Menu`

![Author stats Image](https://i.imgur.com/N4g1jwK.png)

You also able to select multiple authors and fetch general stats

![Authors stats Image](https://i.imgur.com/AwNDJ5H.png)

---

## References
[TEONITE Blog](https://teonite.com/blog/)

[Back To The Top](#teonite-frontend-app)