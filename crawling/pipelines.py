# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import re
import unicodedata
from itemadapter import ItemAdapter
from teonite.models import Teonite


def normalize_char(c):
    try:
        cname = unicodedata.name(c)
        cname = cname[:cname.index(' WITH')]
        return unicodedata.lookup(cname)
    except (ValueError, KeyError):
        return c


def normalize(s):
    return ''.join(normalize_char(c) for c in s)


def clean_words(param):
    word_bank = {}
    filtered_words = []
    for sentence in param:
        filtered_words += re.split(
            ',|\.|‘|’|\(|\)|\:|\ +|\\n|\ -\ |\/|\~|[0-9]+|\!|\?|\-|\&|\_|\#|\%|\+|\@|;|[^\u0000-\u007F]+', sentence)
    for word in filtered_words:
        if word != '':
            if word.lower() in word_bank:
                word_bank[word.lower()] += 1
            else:
                word_bank[word.lower()] = 1
    return word_bank


def generate_authorKey(param):
    name = param.lower().replace(' ', '')
    normalized_name = normalize(name)
    return normalized_name


def concat_words(old_words, new_words):
    for word in new_words:
        if word in old_words:
            old_words[word] = int(old_words[word]) + new_words[word]
        else:
            old_words[word] = new_words[word]

    return old_words


class CrawlingPipeline(object):
    def process_item(self, item, spider):
        authorKey = generate_authorKey(item['author'])
        author = item['author']
        words = clean_words(item['words'])

        objects = Teonite.objects

        if objects.filter(author=author).exists():
            obj = objects.get(author=author)
            obj.words = concat_words(obj.words, words)
            obj.save()
        else:
            objects.create(
                authorKey=authorKey,
                author=author,
                words=words,
            )

        return item
