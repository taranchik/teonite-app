# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from teonite.models import Teonite


class TeoniteItem(DjangoItem):
    django_model = Teonite
    author = scrapy.Field()
    words = scrapy.Field()
