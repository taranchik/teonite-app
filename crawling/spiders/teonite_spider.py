import scrapy
from crawling.items import TeoniteItem
from scrapy.spiders import CrawlSpider


class TeoniteSpider(CrawlSpider):
    name = "teonite"
    allowed_domains = ['teonite.com']
    start_urls = ["https://teonite.com/blog/"]

    def parse(self, response):
        pages = response.xpath('//span[@class="page-number"]/text()').get()
        pages = int(pages.split('/')[1])
        pages += 1
        for page in range(1, pages):
            yield scrapy.http.Request(response.urljoin('../blog/page/' + str(page) + '/index.html'), callback=self.list_posts)

    def list_posts(self, response):
        posts = response.xpath(
            '//div[@class="post-container"]/h2[@class="post-title"]/a/@href').extract()
        for post in posts:
            yield scrapy.http.Request(response.urljoin(post), callback=self.get_data)

    def get_data(self, response):
        item = TeoniteItem()
        item['author'] = response.xpath(
            '//span[@class="author-name"]/strong/text()').get()
        item['words'] = response.xpath(
            "//div[@class='post-content']/p/text()").extract()
        yield item
